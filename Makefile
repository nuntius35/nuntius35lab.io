HTMLS := $(patsubst %.md,public/%.html,$(wildcard *.md))

all: $(HTMLS)

public/%.html: %.md
	pandoc $< -s -o $@ \
	  --include-in-header header.html \
	  --citeproc \
	  --lua-filter add-date.lua \
	  --bibliography mybibliography.bib \
	  --metadata pagetitle="Homepage of Dr. Andreas Geyer-Schulz" \
	  --css pandoc.css \
	  --mathml \
	  --toc
	./add-date.sh

.PHONY: clean rebuild
clean:
	rm -f public/*.html

rebuild: clean all
