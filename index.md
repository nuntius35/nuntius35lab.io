---
author: Andreas Geyer-Schulz
lang: en
link-citations: true
...

# Dr. Andreas Geyer-Schulz

I am a mathematician who is working as a consultant at [d-fine GmbH](https://www.d-fine.com/) in Frankfurt.

I wrote my PhD thesis at the [Institute for Analysis](https://www.math.kit.edu/iana/en), [KIT Department of Mathematics](https://www.math.kit.edu/en), [Karlsruhe Institute of Technology (KIT)](https://www.kit.edu).
During that time I was associated researcher in the collaborative research center on [Wave phenomena: analysis and numerics](https://www.waves.kit.edu).


## Contact data

* E-Mail: [gsa@posteo.de](mailto:gsa@posteo.de) ([PGP-key available](https://posteo.de/keys/gsa@posteo.de))

## PhD thesis

In my thesis I investigate the Maxwell-Schrödinger system in the full space with a power nonlinearity.
I develop a comprehensive local well-posedness theory in suitable spaces above the energy level, which comprises existence, uniqueness, continuous dependence and conserved quantities of solutions to the Cauchy problem.

* [Maxwell--Schrödinger System: Well-Posedness and Standing Waves (pdf/1.1M)](res/Geyer-Schulz_MaxwellSchroedinger_2019-10.pdf), ([DOI: 10.5445/IR/1000100707](https://doi.org/10.5445/IR/1000100707))

## Public outreach

* [Was machen eigentlich Mathematiker? (pdf/532K)](res/Geyer-Schulz_Mathematiker_2016-08.pdf) contribution to the [Science Blog Competition 2016](http://scienceblogs.de/astrodicticum-simplex/2016/09/20/was-machen-eigentlich-mathematiker/).
* [Quadratisch, praktisch, … (pdf/340K)](res/Geyer-Schulz_PiDay_2010-03.pdf) text on the occasion of π-day 2010.
* [Fibonacci im Aufzug (pdf/435K)](res/Geyer-Schulz_PiDay_2009-03.pdf) text on the occasion of π-day 2009.
* [Eine einsame Insel und die Zahl π (pdf/276K)](res/Geyer-Schulz_PiApproxDay_2008-07.pdf) text on the occasion of π-approximation day 2008.

## Scientific Talks

* 18.02.2020, _On global well-posedness of the Maxwell–Schrödinger system_, TULKKA, Konstanz.
* 19.11.2019, _On global well-posedness of the Maxwell–Schrödinger system_, Oberseminar Funktionalanalysis, Karlsruhe.
* 09.10.2019, _Maxwell-Schrödinger system: well-posedness and standing waves_, PhD colloquium, Karlsruhe.
* 09.04.2019, _On the Maxwell–Schrödinger system_, iRTG workshop, Karlsruhe.
* 13.07.2017, _On well-posedness of Maxwell-Schrödinger systems_, CRC seminar, Karlsruhe.
* 15.09.2016, _Minimizing L²-norm with prescribed eigenvalue_, CRC summer school, Karlsruhe.
* 01.06.2015, _Adapted function spaces, spaces of bounded variation and applications_, RTG seminar, Karlsruhe.
* 26.08.2014, _Strichartz and dispersive estimates_, RTG short course, Bad Herrenalb.
* 28.07.2014, _Eigenvalue inequalities in terms of Schatten norm bounds on differences of semigroups and an application to Schrödinger operators_, Workshop of the 17th ISEM, Blaubeuren.
* 22.04.2014, _Eine invariante Mannigfaltigkeit für eine kritische nichtlineare Schrödingergleichung_, Oberseminar Funktionalanalysis, Karlsruhe.
* 13.07.2013, _Minimal smoothness assumptions on the nonlinearity for local wellposedness of semilinear Schrödinger equations_, Workshop of the 16th ISEM, Blaubeuren.
* 04.06.2012, _Nonautonomous equations and evolution families_, Workshop of the 15th ISEM, Blaubeuren.

## Teaching at KIT

* Exercise class for [_Höhere Mathematik 1 für die Fachrichtung Physik_](https://www.math.kit.edu/iana3/lehre/hm1phys2019w/de), winter term 2019/20.
* Exercise class for [_Analysis für das Lehramt_](https://www.math.kit.edu/iana3/lehre/analema2019s/), summer term 2018/19.
* Exercise class for [_Evolutionsgleichungen_](https://www.math.kit.edu/iana3/lehre/evolglgen2018w/), winter term 2018/19.
* Exercise class for [_Analysis 4_](https://www.math.kit.edu/iana3/lehre/ana42018s/), summer term 2018.
* Exercise class for [_Funktionalanalysis_](https://www.math.kit.edu/iana3/lehre/funkana2017w/), winter term 2017/18.
* Exercise class for [_Analysis 4_](https://www.math.kit.edu/iana3/lehre/ana42017s/), summer term 2017.
* Exercise class for [_Analysis 3_](https://www.math.kit.edu/iana3/lehre/ana32016w/), winter term 2016/17.
* Exercise class for [_Analysis 2_](https://www.math.kit.edu/iana3/lehre/ana22016s/), summer term 2016.
* Seminar [_Evolutionsgleichungen_](https://www.math.kit.edu/iana3/lehre/semevolgl2015w/), winter term 2015/16.
* Proseminar [_Populationsdynamik und Differentialgleichungen_](https://www.math.kit.edu/iana3/lehre/prosempopgl2014w/), winter term 2014/15.

## Translations

I attempted to translate the following articles from Russian to English.
The translations are in [this repository](https://gitlab.com/nuntius35/translations).

* F. V. Atkinson: _The normal solubility of linear equations in normed spaces_.
Mat. Sbornik N.S., vol. __28(70)__, №1, 1951, 3–14.
* Ya. Sinaĭ: _The notion of entropy of a dynamical system_.
Dokl. Akad. Nauk SSSR, vol. __124__, №4, 1959, 768–771.
* Ya. Sinaĭ: _Flows with finite entropy_.
Dokl. Akad. Nauk SSSR, vol. __125__, №6, 1959, 1200–1202.

## Book

Tom Lehrer has put his work in the public domain.
I have typeset his lyrics as a beautiful book.

* [Tom Lehrer – Songs and Lyrics (pdf/476K)](res/TomLehrer_Songs.pdf)

## Software

* [Wrong PIN Shutdown](https://f-droid.org/packages/org.nuntius35.wrongpinshutdown/) is a simple Android app available from F-Droid which shutdowns the device after multiple wrong PIN entries.
  The app only works on rooted devices and the [source code](https://gitlab.com/nuntius35/wps) is freely available.
* A mountain peak is called _extremal_ if the tangential plane at the peak does not intersect the earth at any other point.
  A map of all [extremal mountain peaks](https://nuntius35.gitlab.io/extremal_peaks/) based on OpenStreetMap data can be created with this [code](https://gitlab.com/nuntius35/extremal_peaks).
* Some pharmacies promote alternative medicine more heavily on their websites than others.
  I created a [map](https://nuntius35.gitlab.io/pharmacies/) which visualizes these differences.
  The map is created from this [code](https://gitlab.com/nuntius35/pharmacies) and is based on OpenStreetMap data and rough web crawling.
* At the [AG DANK 2015](https://em.iism.kit.edu/ag-dank2015/) a data set from an online car configurator was presented.
  I created a simple [visualization](https://gitlab.com/nuntius35/carconfig) of 450 000 car configurations.

## Links

* [My GitLab profile](https://gitlab.com/nuntius35){rel="me"}
* [My GitHub profile](https://github.com/nuntius35){rel="me"}
* [My Mathoverflow profile](https://mathoverflow.net/users/50551/gsa){rel="me"}
* [My OpenStreetMap profile](https://www.openstreetmap.org/user/gsa){rel="me"}
* [My homepage at KIT](https://www.math.kit.edu/grk1294/~gsa/){rel="me"}
* [My mathematical genealogy](https://genealogy.math.ndsu.nodak.edu/id.php?id=257168)

---
Last update: TODAYSDATE
